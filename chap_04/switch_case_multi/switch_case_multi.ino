/*
Both 1 and 4 cases will be executed, till first `break` or all case statements
are finished.
*/
void setup() {
  Serial.begin(57600);
}

void loop() {
  // int j = 4;

  int j = 1;
  switch (j) {
    case 0:
      Serial.println("0");
      break;
    case 1:
      Serial.println("1");
      // break;
    case 4:
      Serial.println("4");
      break;
    default:
      // if nothing else matches, do the default
      // default is optional
      Serial.println("Default");
      break;
  }
  Serial.println("Done");
}