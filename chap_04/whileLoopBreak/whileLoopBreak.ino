void setup() {
  Serial.begin(57600);
}

void loop() {
  int j = 4;
  while(j > 0){
    Serial.print("My variable is: ");
    Serial.println(j--);
    if (j == 2){
      break;
    }
  }
  Serial.println("Done");
}